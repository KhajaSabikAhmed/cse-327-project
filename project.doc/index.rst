.. cse327 documentation master file, created by
   sphinx-quickstart on Mon Mar 15 23:36:13 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

RetroCoders Project Documentation
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   source/introduction
   source/overall_description
   source/external_interface_requirements
   source/system_features
   source/use_case_diagram
   source/expended_use_cases_with_class_designs
   source/other_nonfunctional_requirements
   source/other_requirements
   
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
