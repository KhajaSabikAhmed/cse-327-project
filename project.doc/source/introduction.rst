=====================
   Introduction
=====================


---------------
 Purpose
---------------
This project aims to digitize existing documents/files from the Department of Finance. It will be 
transferred to digital format and stored in a location determined by the Department of Finance, 
where it will be attached using a simple document system capable of search and retrieval. It will
explain the system's purpose and features, the interfaces of the system, what the system will do, 
the constraints under which it must operate, and how the system will react to external stimuli. This 
document is intended for both the admin and the developers of the system and proposed to the respective 
Faculty for its approval.


----------------------------
 Document Conventions
----------------------------
Every requirement statement should be prioritized.


--------------------------------------------------
 Intended Audience and Reading Suggestions
--------------------------------------------------
This paper is intended for both admin and device creators, and it will be sent for approval to the 
appropriate Faculty.


---------------------
 Product Scope
---------------------
This software is Online Document Digitization System for Plan International Bangladesh. This system
will be designed to convert the original paper document into electronic/ digital format using a 
practical, cost-efficient, and technologically advanced document digitization solution. It will offer 
easy and straightforward Instructions for the new users. Additionally, the digitized document will be 
able to integrate with the existing document management system of the Department without using 
any third-party software. Users can upload their files/documents in this cloud-based system. Also, 
users can select files as personal and public files. The system also contains a relational database 
containing a list of documents/ files of different finance department sectors.

-------------------
 References
-------------------

Not Applicable