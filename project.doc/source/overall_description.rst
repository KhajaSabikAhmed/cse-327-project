=========================================
   Overall Description
=========================================



------------------------------
 Product Perspective
------------------------------
This System is a new, self-combined product. It consists of a mobile application and website integrated 
with it to view all the files the stores in cloud folders. All folders have relevant information about 
files and also the option to make custom folders depending on the security.


------------------------------
 Product Functions
------------------------------
| ● User Registration (Verify via email or mobile message) using API
| ● User Login 
| ● Login with Google account
| ● Create user account with specific role
| ● Reset password
| ● Upload avatar
| ● Create group
| ● Create Folders
| ● Upload and View files
| ● Lock/Unlock files
| ● Upload an index file in CSV format and automatically link the attributes in designated file names without any limit.
| ● Index each record
| ● Top-rated files 
| ● Search data
| ● Search ocr of scanned document.
| ● Search with filter. View real-time notification in dashboard.
| ● Customer support chatbot using API 
| ● verification using API
| ● Filter service Based on Budget
----------------------------------------------
 User Classes and Characteristics
----------------------------------------------
+--------+------------------------------------------------------------------------------+
| User   | Can create folders, upload files, lock files, search files and upload avatar |
+========+==============================================================================+
| Admin  | Manage the system                                                            |
+--------+------------------------------------------------------------------------------+
| System | Authentication, push notifications, generate rated suggestions               |
+--------+------------------------------------------------------------------------------+
    


--------------------------------
 Operating Environment
--------------------------------
| ● Operating System: Android / Windows. 
| ● Database: Firebase Firestore and Firebase Realtime Database
| ● System: server system
| ● platform: Android and Web 

-----------------------------------------------
 Design and Implementation Constraints
-----------------------------------------------
| ● Designing the database system
| ● Implementation of proper api usage 
| ● Designing complex relational data dependencies 

--------------------------------
 User Documentation
--------------------------------
 Not Applicable

--------------------------------------
 Assumptions and Dependencies
--------------------------------------
| ● Availability of the proper uploading files
| ● Availability of proper searching keywords.
| ● Availability of minimum file uploading requirements.
| ● Availability of sufficient resource ratings in system for user recommendation.