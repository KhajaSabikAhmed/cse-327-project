===================================
External Interface Requirements
===================================



------------------------
 User Interfaces
------------------------
| ● Frontend : Java , Css , Html .
| ● Backend  : Django , Firebase , Java . 


---------------------------
 Hardware Interfaces
---------------------------
The system won't have any hardware interfaces. Because the android app or the website won’t 
need any special consideration from the program.


----------------------------
 Software Interfaces
----------------------------
+------------------+---------------------------------------------------+
| Operating System | Android                                           |
+==================+===================================================+
| Database         | Firebase firestore and Firebase realtime database |
+------------------+---------------------------------------------------+
| Framework        | Django                                            |
+------------------+---------------------------------------------------+


---------------------------------
 Communications Interfaces
---------------------------------
This product will run on all the web browsers and doesn’t need any special communication method