=====================================
Other Nonfunctional Requirements
=====================================

-----------------------------------
	Performance Requirements
-----------------------------------
This section provides a detailed specification of the user interaction with the software and the time 
measurement based on the system’s performance.

**Performance requirements are:**
  •	Ambiguous: The system will load with in an adequate time.
  •	Unrealistic: The system will not load less than 0.01 seconds at all the time.
  •	User Interface (UI): The system’s UI must be flexible, user-friendly and responsive.
  •	Unverifiable; The system must be safe, secure, portable, lightweight and fast. 


-----------------------------------
 Safety Requirements
-----------------------------------
This section provides a detailed specification of the user interaction with the software and the time 
measurement based on the system’s performance.
To prevent data from getting lost all the information must be uploaded in the cloud storage. So, if 
the user’s device is damaged or lost or he changes his device he can log in with his account and 
restore all his information. This cloud storage will allow safe and easy access from anywhere.


------------------------------
 Security Requirements
------------------------------
The system shall not disclose any personal information of the user. The system will be able to 
deny access of unauthorized visitors. Also, system will not allow multiple access of the visitors at 
the same time. No unauthorized device can access the system and it must not communicate with any other 
device or server while user is in use In addition, sensitive information’s like credit card information, 
photo id or any other information must be safe and secured and should be stored in the system in encrypted 
form. 


-------------------------------------
 Software Quality Attributes
-------------------------------------
   • Availability: Folder/File creation will be available on demand of the user. Also, other services like upload, lock, unlock service will be available based on user.
   • Correctness: The information about the files will be in correct form depending on the uploading criteria.
   • Maintainability: The price of the locked files and other expenditure will be updated periodically.
   • Usability: Our system will be able to satisfy a maximum number of customers and will give them one stop solution.
   

------------------------
 Business Rules
------------------------
This application will be free for the users. And for the locked file services we will not initially charge them 
but as the popularity grows and more people uses our system then we will charge a little from the users.


