=================================
  System Features
=================================



-----------------------------
 User Registration
-----------------------------
   
      	Description and Priority
   --------------------------------------
      **Description**: User registers themselves via the app or website. The user credentials will 
      be saved to the Firebase authentication database.      

      **Priority**: High

   	Stimulus/Response Sequences
   ----------------------------------------
     • Enter the android app / the website. 
     • If first time, then sign up using credentials. If already registered, log in with credentials. 
     • Firebase authentication verifies the credentials and lets the actor into the home page.

   	Functional Requirements
   --------------------------------------
     • In case of wrong credentials, the system prompts the actor for the correct credentials.
     • If the server is busy or disconnected. The system would wait and reconnect as quickly as possible.
                   
   


-------------------------------------
 Create Folder
-------------------------------------
       Description and Priority
   ----------------------------------------

      **Description**: After the actor logs in, they will be taken to the home screen, 
      where they can create folders and then choose upload files.

      **Priority**: High

       Stimulus/Response Sequences
   -----------------------------------------
    •	Enter the android app / the website. 
    •	If first time, then sign up using credentials. If already registered, log in with credentials. 
    •	Firebase authentication verifies the credentials and lets the actor into the home page.
    •	From home actor select create folder option. Then selects preferred uploading files. Uploaded files are visible in the created folders. 

       Functional Requirements
   -------------------------------------
    •	In case of wrong credentials, the system prompts the actor for the correct credentials.
    •	If the server is busy or disconnected. The system would wait and reconnect as quickly as possible.

------------------------
 Uploads files
-------------------------
     	Description and Priority
   -------------------------------------
         **Description**: After choosing the upload files option, the actor will now select files from 
         their local disk and upload them.

     **Priority**: High

        Stimulus/Response Sequences
    ----------------------------------------
    •	Enter the android app / the website. 
    •	If first time, then sign up using credentials. If already registered, log in with credentials. 
    •	Firebase authentication verifies the credentials and lets the actor into the home page.  
    •	After choosing upload files actor can upload files.

   	Functional Requirements
   ------------------------------------
    •	In case of wrong credentials, the system prompts the actor for the correct credentials.
    •	If the server is busy or disconnected. The system would wait and reconnect as quickly as possible.
    •	If the uploading files are exceeding maximum limits, an error message occurred and suggested checking 
      the file size.

-------------------------------
 Public/ Private files
-------------------------------
    	Description and Priority
   ------------------------------------
         **Description**: The actor will be able to select either their files are private or public.
         **Priority**: high

     	Stimulus/Response Sequences
   -----------------------------------------
    •	Enter the android app / the website. 
    •	If first time, then sign up using credentials. If already registered, log in with credentials. 
    •	Firebase authentication verifies the credentials and lets the actor into the home page.  
    •	Public/Private files option will show once the actor has chosen the files security option.


       Functional Requirements
   ------------------------------------
    • In case of wrong credentials, the system prompts the actor for the correct credentials.
    • If the server is busy or disconnected. The system would wait and reconnect as quickly as possible.

-----------------------------------
 Customer Support Chat Bot
-----------------------------------
      	Description and Priority
   -------------------------------------
         **Description**: The actor can seek help from the customer about using the app or website, 
         and the chatbot will reply accordingly. The chatbot will also show suggestions if the user asks.

         **Priority**: High


        Stimulus/Response Sequences
    ----------------------------------------
    •	Enter the android app / the website. 
    •	If first time, then sign up using credentials. If already registered, log in with credentials. 
    •	Firebase authentication verifies the credentials and lets the actor into the home page.  
    •	From the home page, the user can access the chatbot to talk to it.

       Functional Requirements
    ------------------------------------
    •	In case of wrong credentials, the system prompts the actor for the correct credentials.
    •	If the server is busy or disconnected. The system would wait and reconnect as quickly as possible.
    •	If the chatbot doesn't know what the user wants, the bot will tell the user to mail. The mail option will take the user to their mail client. 




    