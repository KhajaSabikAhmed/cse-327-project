=========================
Other Requirements
=========================

   Not Applicable 

**Appendix A: Glossary**

+-----------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Framework | It helps to build the project compactly as possible. Puts all the tools together for better managing the project                                                                               |
+===========+================================================================================================================================================================================================+
| Backend   | The behind the scenes part of the project. The user never sees this part. The developers that manage the system has access to it and can make change to the program accordingly to the demand. |
+-----------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

**Appendix B: Analysis Models**

     Not Applicable

**Appendix C: To Be Determined List**

  Payment option is TBD and will require some looking into it. Fix will come as soon as the problem is fixed with 
  authorization.   