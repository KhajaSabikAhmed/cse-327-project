from django.db import models
from django.contrib.auth.models import User


#Model for apartment adverts
class UserProfile (models.Model):
    first_name = models.TextField()
    last_name = models.TextField(default=None, blank=True)
    email = models.TextField()
    picture = models.ImageField(upload_to='pictures/%y/%m/%d/', max_length=255, null=True, blank=True)

