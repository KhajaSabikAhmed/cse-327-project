from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import UserForm

def index(request):
   return render(request, 'home.html')


def createuser(request):
   form = UserForm(request.POST, request.FILES)
   if form.is_valid():
      form.save()
      return redirect('home')
   context = {'form': form}
   return render(request, 'user.html', context)